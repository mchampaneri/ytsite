package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"

	"github.com/fatih/color"
	geoip2 "github.com/oschwald/geoip2-golang"
)

func home(w http.ResponseWriter, r *http.Request) {
	View(w, r, nil, "index2.html")
}

func indexPage(w http.ResponseWriter, r *http.Request) {
	UserSession.Get(r, "mvc-user-session")
	// getting requtes host ip adddress
	db, err := geoip2.Open("GeoIP2.mmdb")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	color.Red(r.Header.Get("X-Forwarded-For"))
	ip := net.ParseIP(r.Header.Get("X-Forwarded-For"))
	if ip == nil {
		ip = net.IPv4(100, 100, 100, 100)
	}
	record, err := db.City(ip)
	if err != nil {
		// log.Fatal(err)
		DefaultLogger.Error("Error during gettin loaction via ip " + err.Error())
	}
	dataMap := make(map[string]interface{})
	dataMap["country"] = record.Country.IsoCode
	dataMap["promote"], _ = json.Marshal(Promote.Slots)
	fmt.Println(dataMap["promote"])
	View(w, r, dataMap, "index.html")
}

func bobM(w http.ResponseWriter, r *http.Request) {
	UserSession.Get(r, "mvc-user-session")
	View(w, r, nil, "bobm.html")
}
