package main

import (
	"fmt"
	"net/http"

	"github.com/dghubble/gologin/google"
	oauth2Login "github.com/dghubble/gologin/oauth2"
	"golang.org/x/oauth2"
	googleOAuth2 "golang.org/x/oauth2/google"
	blogger "google.golang.org/api/blogger/v2"
)

type SocialController struct{}

var Social SocialController

var GpConf *oauth2.Config

func init() {

	GpConf = &oauth2.Config{
		ClientID:     "494540788863-6ps3kpe1e4mjnqc3ddm18klctgl260dr.apps.googleusercontent.com",
		ClientSecret: "8U7jXXXqZfK4eLorMDkJdWxA",
		RedirectURL:  "http://localhost:8081/gp/callback",
		Scopes:       []string{blogger.BloggerScope},
		Endpoint:     googleOAuth2.Endpoint,
	}
}

func (SocialController) GPissueSession() http.Handler {

	fn := func(w http.ResponseWriter, req *http.Request) {

		ctx := req.Context()

		TokenToUse, _ := oauth2Login.TokenFromContext(ctx)

		gpuser, err := google.UserFromContext(ctx)

		usersession, errS := UserSession.Get(req, "mvc-user-session")
		if errS != nil {
			fmt.Println(errS.Error(), " during accessing the session")
		}
		usersession.Values["AccessToken"] = TokenToUse.AccessToken
		usersession.Values["RefreshToken"] = TokenToUse.RefreshToken
		usersession.Values["Id"] = gpuser.Id
		usersession.Values["Sample"] = "this is the sample"
		usersession.Save(req, w)

		if err != nil {
			fmt.Println("Error at issuing the token", err.Error())
			return
		}

		http.Redirect(w, req, "/blogs", http.StatusMovedPermanently)

	}
	return http.HandlerFunc(fn)
}
