package main

import (
	"github.com/gorilla/mux"
	"github.com/otium/ytdl"
)

type FormatResponse struct {
	Data ytdl.Format
	Qr   []byte
}

func dynamicRoutes(router *mux.Router) {

	router.HandleFunc("/", home)

	// router.HandleFunc("/bobmovies", bobM)

	// router.HandleFunc("/downloadany", func(w http.ResponseWriter, r *http.Request) {

	// 	url := r.FormValue("url")

	// 	client := http.Client{}
	// 	resp, err := client.Get(url)

	// 	if err != nil {
	// 		fmt.Println("error during client downloading file", err.Error())
	// 		return
	// 	}

	// 	w.Header().Set("Content-Disposition", "attachment; filename=file.mp4")
	// 	w.Header().Set("Content-Type", r.Header.Get("Content-Type"))
	// 	w.Header().Set("Content-Length", strconv.Itoa(int(resp.ContentLength)))
	// 	io.Copy(w, resp.Body)

	// })

	// router.HandleFunc("/download", func(w http.ResponseWriter, r *http.Request) {
	// 	r.ParseForm()
	// 	if r.FormValue("url") == "" {
	// 		fmt.Fprintln(w, "please atleast give a url bro ! ")
	// 		return
	// 	}

	// 	if r.Header.Get("X-Requested-With") == "XMLHttpRequest" {

	// 		fmt.Println(r.Header.Get("X-Requested-With"))
	// 		vid, _ := ytdl.GetVideoInfo(r.FormValue("url"))

	// 		dataMap := make(map[string]interface{})
	// 		// dataMap["formats"] = vid.Formats
	// 		var formatMap []FormatResponse

	// 		for _, format := range vid.Formats {

	// 			png, err := qrcode.Encode(fmt.Sprint("/hsd?url=", url.PathEscape(r.FormValue("url")),
	// 				"&format=", format.Itag), qrcode.High, 512)

	// 			if err != nil {
	// 				// color.Red("can not create qr code for given url ", err.Error())
	// 				DefaultLogger.Error("can not create qr code for given url" + err.Error())
	// 				return
	// 			}
	// 			newFormat := FormatResponse{
	// 				Data: format,
	// 				Qr:   png,
	// 			}
	// 			formatMap = append(formatMap, newFormat)
	// 		}

	// 		dataMap["formats"] = formatMap
	// 		dataMap["url"] = r.FormValue("url")
	// 		dataMap["title"] = vid.Title
	// 		dataMap["description"] = vid.Description
	// 		dataMap["thumb"] = vid.GetThumbnailURL(ytdl.ThumbnailQualityHigh).String()

	// 		// fmt.Println(dataMap["formats"])
	// 		JSON(w, dataMap)
	// 		return
	// 	} else {
	// 		db, err := geoip2.Open("GeoIP2.mmdb")
	// 		if err != nil {
	// 			// log.Fatal(err)
	// 			DefaultLogger.Error("Error during gettin loaction via ip " + err.Error())
	// 		}
	// 		defer db.Close()
	// 		// color.Red(r.Header.Get("X-Forwarded-For"))
	// 		ip := net.ParseIP(r.Header.Get("X-Forwarded-For"))
	// 		if ip == nil {
	// 			ip = net.IPv4(100, 100, 100, 100)
	// 		}
	// 		record, err := db.City(ip)
	// 		if err != nil {
	// 			// log.Fatal(err)
	// 			DefaultLogger.Error("Error during gettin loaction via ip " + err.Error())
	// 		}
	// 		// color.HiGreen("returning View")
	// 		dataMap := make(map[string]interface{})
	// 		dataMap["url"] = r.FormValue("url")
	// 		dataMap["country"] = record.Country.IsoCode
	// 		dataMap["promote"], _ = json.Marshal(Promote.Slots)
	// 		View(w, r, dataMap, "index.html")
	// 		return
	// 	}

	// })

	// router.HandleFunc("/watch", func(w http.ResponseWriter, r *http.Request) {

	// 	if r.FormValue("v") == "" {
	// 		fmt.Fprintln(w, "please atleast give a url bro ! ")
	// 		return
	// 	}

	// 	db, err := geoip2.Open("GeoIP2.mmdb")
	// 	if err != nil {
	// 		// log.Fatal(err)
	// 		DefaultLogger.Error("Error during opening GeoIP2 db" + err.Error())
	// 	}
	// 	defer db.Close()
	// 	color.Red(r.Header.Get("X-Forwarded-For"))
	// 	ip := net.ParseIP(r.Header.Get("X-Forwarded-For"))
	// 	if ip == nil {
	// 		ip = net.IPv4(100, 100, 100, 100)
	// 	}
	// 	record, err := db.City(ip)
	// 	if err != nil {
	// 		// log.Fatal(err)
	// 		DefaultLogger.Error("Error during gettin loaction via ip " + err.Error())
	// 	}
	// 	// color.HiGreen("returning View")
	// 	dataMap := make(map[string]interface{})
	// 	dataMap["url"] = r.FormValue("url")
	// 	dataMap["country"] = record.Country.IsoCode
	// 	dataMap["url"] = fmt.Sprint("https://youtube.com", r.URL)

	// 	View(w, r, dataMap, "index.html")
	// })

	// router.HandleFunc("/hsd", func(w http.ResponseWriter, r *http.Request) {

	// 	if r.FormValue("url") == "" {
	// 		fmt.Fprintln(w, "please atleast give a url bro ! ")
	// 		return
	// 	}

	// 	vid, _ := ytdl.GetVideoInfo(r.FormValue("url"))
	// 	formats := vid.Formats
	// 	index, _ := strconv.Atoi(r.FormValue("format"))

	// 	for _, format := range formats {
	// 		if format.Itag == index {
	// 			reg, err := regexp.Compile("[^a-zA-Z0-9]+")
	// 			if err != nil {
	// 				DefaultLogger.Error("Regex Error " + err.Error())
	// 			}
	// 			processedString := reg.ReplaceAllString(vid.Title, "")
	// 			url, _ := vid.GetDownloadURL(format)
	// 			dataMap := make(map[string]interface{})
	// 			dataMap["url"] = url
	// 			dataMap["name"] = fmt.Sprint(processedString, ".", format.Extension)
	// 			resp, _ := http.Get(url.String())
	// 			w.Header().Set("Content-Disposition", "attachment; filename="+processedString+"."+format.Extension)
	// 			w.Header().Set("Content-Type", r.Header.Get("Content-Type"))
	// 			w.Header().Set("Content-Length", strconv.Itoa(int(resp.ContentLength)))
	// 			if _, err := io.Copy(w, resp.Body); err != nil {
	// 				if errBody := resp.Body.Close(); errBody != nil {
	// 					return
	// 				}
	// 				return
	// 			}
	// 			defer resp.Body.Close()
	// 		}
	// 	}
	// })

	// router.HandleFunc("/hsd2", func(w http.ResponseWriter, r *http.Request) {

	// 	if r.FormValue("url") == "" {
	// 		fmt.Fprintln(w, "please atleast give a url bro ! ")
	// 		return
	// 	}

	// 	vid, _ := ytdl.GetVideoInfo(r.FormValue("url"))
	// 	formats := vid.Formats
	// 	index, _ := strconv.Atoi(r.FormValue("format"))

	// 	for _, format := range formats {
	// 		if format.Itag == index {

	// 			reg, err := regexp.Compile("[^a-zA-Z0-9]+")
	// 			if err != nil {
	// 				DefaultLogger.Error("Regex Error " + err.Error())
	// 			}
	// 			processedString := reg.ReplaceAllString(vid.Title, "")

	// 			urlToDownload, _ := vid.GetDownloadURL(format)
	// 			values, _ := url.ParseQuery(urlToDownload.RawQuery)

	// 			values.Set("ip", r.Header.Get("X-Forwarded-For"))

	// 			urlToDownload.RawQuery = values.Encode()
	// 			// TODO: Try to remove below block of code
	// 			// [ unncesary consumption of bandwidth ]
	// 			dataMap := make(map[string]interface{})
	// 			dataMap["url"] = urlToDownload.String()
	// 			dataMap["name"] = fmt.Sprint(processedString, ".", format.Extension)
	// 			JSON(w, dataMap)
	// 		}
	// 	}
	// })

	// router.HandleFunc("/bobm", func(w http.ResponseWriter, r *http.Request) {

	// 	boburl := r.FormValue("url")
	// 	fmt.Println(boburl)
	// 	fmt.Println("---------------- ")

	// 	bow := surf.NewBrowser()
	// 	err := bow.Open(boburl)
	// 	if err != nil {
	// 		panic(err)
	// 	}

	// 	s := bow.Find("a.tab-server3").First()
	// 	a, _ := s.Attr("data-url")

	// 	// color.Yellow(a)

	// 	bow2 := surf.NewBrowser()
	// 	err2 := bow2.Open(a)
	// 	if err != nil {
	// 		// fmt.Println("fail to open the url", err2.Error())
	// 		DefaultLogger.Error("fail to open the url " + err2.Error())
	// 		return
	// 	}

	// 	d := bow2.Find("source").First()
	// 	durl, _ := d.Attr("src")

	// 	// color.Yellow(durl)
	// 	dataMap := make(map[string]interface{})
	// 	dataMap["url"] = durl

	// 	JSON(w, dataMap)

	// 	// client := http.Client{}
	// 	// resp, err := client.Get(durl)

	// 	// if err != nil {
	// 	// 	fmt.Println("error during client downloading file", err.Error())
	// 	// 	return
	// 	// }

	// 	// w.Header().Set("Content-Disposition", "attachment; filename=file.mp4")
	// 	// w.Header().Set("Content-Type", r.Header.Get("Content-Type"))
	// 	// w.Header().Set("Content-Length", strconv.Itoa(int(resp.ContentLength)))
	// 	// io.Copy(w, resp.Body)

	// 	// Outputs: "The Go Programming Language"

	// })

	// router.HandleFunc("/share", func(w http.ResponseWriter, r *http.Request) {
	// 	if r.FormValue("url") == "" {
	// 		fmt.Fprintln(w, "please atleast give a url bro ! ")
	// 		return
	// 	}

	// 	vid, _ := ytdl.GetVideoInfo(r.FormValue("url"))

	// 	// return

	// 	dataMap := make(map[string]interface{})
	// 	// wd, _ := os.Getwd()

	// 	// file, _ := os.OpenFile(fmt.Sprint(wd, vid.Title+"."+format.Extension), os.O_CREATE, os.ModePerm)
	// 	dataMap["metaImage"] = vid.GetThumbnailURL(ytdl.ThumbnailQualityHigh).String()
	// 	dataMap["metaTitle"] = vid.Title
	// 	dataMap["description"] = vid.Description
	// 	dataMap["url"] = fmt.Sprint("/download?url=", r.FormValue("url"))
	// 	// downloadeError := vid.Download(format, w)

	// 	View(w, r, dataMap, "share.html")

	// })

}
